﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ReSharper disable RedundantDefaultMemberInitializer

namespace Player {
    public class HoleManager : MonoBehaviour {
        private List<GameObject> _holes = new List<GameObject>();
        [SerializeField] private Wheel wheel = null;
        [SerializeField] private Crosses crosses = null;
        [SerializeField] private int bonusHpOnFixHole = 2;
        private List<float> _coolDowns = new List<float>();
        private const float CoolDownHole = 1f;
        [SerializeField] private AudioClip beginSFX = null;
        [SerializeField] private AudioClip loopSFX = null;
        [SerializeField] private AudioClip fixedHoleSFX = null;
        [SerializeField] private AudioSource holesAudioSource = null;
        [SerializeField] private AudioSource fixedHolesAudioSource = null;

        public void AddHole(GameObject go) {
            _holes.Add(go);
            _coolDowns.Add(CoolDownHole);
            if (_holes.Count <= 1)
            {
                StartCoroutine(playFirstHole());
            }
        }
        
        IEnumerator playFirstHole()
        {
            holesAudioSource.clip = beginSFX;
            holesAudioSource.Play();
            yield return new WaitForSeconds( holesAudioSource.clip.length);
            holesAudioSource.clip = loopSFX;
            holesAudioSource.loop = true;
            holesAudioSource.Play();

        }

        public void Reset()
        {
            _holes.Clear();
            holesAudioSource.Stop();
            holesAudioSource.clip = null;
        }

        public void FixHole(GameObject go)
        {
            _coolDowns.Remove(_holes.IndexOf(go));
            _holes.Remove(go);
            wheel.pv += bonusHpOnFixHole;
            fixedHolesAudioSource.Play();
            if (_holes.Count <= 0) holesAudioSource.Stop();
            Destroy(go);
        }

        public void FixedUpdateLoop() {
            int holesCount = _holes.Count;
            for (int i = 0; i < holesCount; i++) {
                if (_coolDowns[i] < 0) {
                    wheel.pv -= 1;
                    _coolDowns[i] = CoolDownHole;
                }
                _coolDowns[i] -= Time.fixedDeltaTime;
            }
            wheel.UpdateScale();
            if (wheel.pv <=0 || crosses.chances <= 1) holesAudioSource.Stop();

        }
    }
}
