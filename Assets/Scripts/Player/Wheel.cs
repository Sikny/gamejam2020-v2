﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using DG.Tweening;
using Global;
using UnityEngine;
using Random = UnityEngine.Random;

// ReSharper disable RedundantDefaultMemberInitializer

namespace Player {
    public class Wheel : MonoBehaviour {
        [SerializeField] private float wheelSpeed = 1f;
        [SerializeField] private int maxPv = 100;
        [SerializeField] public int pv;
        [SerializeField] private List<Transform> rotatingParts = null;
        [SerializeField] private List<Transform> scalingParts = null;
        [SerializeField] private GameObject holePrefab = null;
        [SerializeField] private Transform holesParent = null;
        [SerializeField] private Collider2D holesSpawnCircleMin = null;
        [SerializeField] private Collider2D holesSpawnCircleMax = null;
        [SerializeField] private HoleManager holeManager = null;
        [SerializeField] private HandController handController = null;
        public GameManager gameManager;

        [SerializeField] private float minScale = 0f;
        private Vector2 _minScaleVec;
        [SerializeField] private float maxScale = 1f;
        private Vector2 _maxScaleVec;

        public float radius;

        private void Awake() {
            pv = maxPv;
        }

        public void Reset()
        {
            pv = maxPv;
        }

        void Start() {
//            foreach (Transform part in rotatingParts) {
//                part.DORotate(Vector3.back,  gameManager.globalSpeed)
//                    .SetSpeedBased()
//                    .SetRelative()
//                    .SetLoops(-1, LoopType.Incremental);
//            }
            
            _minScaleVec = new Vector2(minScale, minScale);
            _maxScaleVec = new Vector2(maxScale, maxScale);
        }

        public void UpdateScale() {
            foreach (Transform t in scalingParts) {
                t.DOScale(Vector2.Lerp(_minScaleVec, _maxScaleVec, pv / (float) maxPv), 0.25f);
                if (pv <=0) gameManager.LooseGame();
            }
        }

        public void FixedUpdate()
        {
            float angularSpeed = gameManager.globalSpeed/radius;
            float degreeSpeed = angularSpeed * 57.3f;
            Vector3 rotation = -Vector3.forward * angularSpeed;
            foreach (Transform part in rotatingParts) {
                part.Rotate(rotation,Space.Self);
                part.Rotate(part.position,angularSpeed*Time.fixedDeltaTime);  
            }

        }

        public void SetHole(Vector2 pos) {
            GameObject hole = Instantiate(holePrefab, holesParent);
            holeManager.AddHole(hole);
            Vector2 minPos = holesSpawnCircleMin.ClosestPoint(pos);
            Vector2 maxPos = holesSpawnCircleMax.ClosestPoint(pos);
            float r = Random.Range(0f, 1f);
            hole.transform.position = Vector2.Lerp(minPos, maxPos, r);
            hole.GetComponent<SpriteRenderer>().sortingOrder = handController.holeFixerCount++;
        }
    }
}
