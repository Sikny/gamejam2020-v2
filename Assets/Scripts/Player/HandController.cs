﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using Random = UnityEngine.Random;

// ReSharper disable RedundantDefaultMemberInitializer

namespace Player {
    public class HandController : MonoBehaviour {
        [SerializeField] private float bpm = 60f;
        [SerializeField] private float tapSpeed = 1f;
        [SerializeField] private Transform holeFixersParent = null;
        [SerializeField] private Transform fingerTip = null;
        [SerializeField] private List<HoleFixer> fixHolePrefabs = null;
        [SerializeField] private HoleManager holeManager = null;
        [SerializeField] private Crosses crossesController = null;
        [SerializeField] private AudioSource handAudioSource = null;
        [SerializeField] private AudioSource fixedHolesAudioSource = null;
        [SerializeField] private AudioClip miss = null;
        [SerializeField] private AudioClip touchHoles = null;
        
        private Vector3 _wheelTouchPos;
        private TweenerCore<Vector3, Vector3, VectorOptions> _tweenIdle;
        private TweenerCore<Vector3, Vector3, VectorOptions> _tweenTap;

        [HideInInspector] public int holeFixerCount = 20;

        void Start() {
            float bps = bpm / 60f;
            Transform selfTransform;
            _tweenIdle = (selfTransform = transform).DOLocalMove((Vector3.left + Vector3.up).normalized, 1f/(2*bps))
                .SetRelative()
                .SetLoops(-1, LoopType.Yoyo);
            _wheelTouchPos = selfTransform.localPosition+selfTransform.right*selfTransform.localScale.x;
        }

        public void UpdateLoop() {
            if (Input.GetKeyDown(KeyCode.A))
            {
                handAudioSource.Play();
                _tweenIdle.Pause();
                _tweenTap.Kill();
                _tweenTap = transform.DOLocalMove(_wheelTouchPos, tapSpeed)
                    .SetSpeedBased()
                    .OnComplete(() => {
                        // hole fixer instantiate
                        HoleFixer go = Instantiate(fixHolePrefabs[Random.Range(0, fixHolePrefabs.Count)], holeFixersParent);
                        go.transform.position = fingerTip.position;
                        // auto place if hole
                        var hit = Physics2D.Raycast(fingerTip.position, 
                            holeFixersParent.position-fingerTip.position, 10f, 1 << 11);
                        if (hit) {
                            go.transform.position = hit.collider.transform.position;
                            go.spriteRenderer.sortingOrder = hit.collider.transform.GetComponent<SpriteRenderer>().sortingOrder+1;
                            holeManager.FixHole(hit.collider.gameObject);
                            fixedHolesAudioSource.clip = touchHoles;
                            fixedHolesAudioSource.Play();
                        }
                        else
                        {
                            crossesController.MissClick();
                            fixedHolesAudioSource.clip = miss;
                            fixedHolesAudioSource.Play();
                        }
                        go.transform.Rotate(Vector3.back*Random.Range(0f, 180f));
                        go.spriteRenderer.sortingOrder = holeFixerCount++;
                        _tweenIdle.Play();
                    });
            }
        }
    }
}













