﻿using Player;
using UnityEngine;

public class Mask : MonoBehaviour
{
    public void Reset()
    {
        foreach (HoleFixer hf in GetComponentsInChildren<HoleFixer>())
        {
            Destroy(hf.gameObject);
        }
        
        foreach (SpriteRenderer hole in GetComponentsInChildren<SpriteRenderer>())
        {
            Destroy(hole.gameObject);
        }
    }
}
