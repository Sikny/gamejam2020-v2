﻿using System;
using System.Collections.Generic;
using System.Linq;
using Global;
using Parallax;
using UnityEngine;
using UnityEngine.Serialization;

// ReSharper disable RedundantDefaultMemberInitializer

namespace Parallax {
    public class ParallaxManager : MonoBehaviour
    {
        public List<ParallaxLayer> layers;
        [SerializeField] private Vector3 motion = Vector3.zero;
        public GameManager gameManager;

        private void Start()
        {
            foreach (ParallaxLayer layer in layers)
            {
                layer.gameManager = gameManager;
            }
        }

        public void FixedUpdateLoop () {
            foreach (ParallaxLayer layer in layers)
            {
                layer.Move(motion);
            }
        }
    }
}
