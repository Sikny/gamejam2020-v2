﻿using UnityEngine;

namespace Parallax {
    public class Move : MonoBehaviour
    {
        [SerializeField] private float speed = .2f;

        public float Speed => speed;
    }
}
