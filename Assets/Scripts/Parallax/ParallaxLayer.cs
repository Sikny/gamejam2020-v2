﻿using System;
using System.Collections.Generic;
using Global;
using UnityEngine;

namespace Parallax {
    [Serializable]
    public class ParallaxLayer : MonoBehaviour
    {
        public float gain;
        [SerializeField] public List<SpriteRenderer> images;

        public GameManager gameManager;
        private float _cameraHalfWidth;
        
        private void Awake() {
            var main = Camera.main;
            if (main != null) _cameraHalfWidth = main.orthographicSize * main.aspect;
        }

        public void Move(Vector3 motion)
        {
            float dist = motion.x * gain;
            transform.position += Vector3.right * dist * Time.fixedDeltaTime * gameManager.globalSpeed;
            foreach (SpriteRenderer image in images)
            {
                //On récupère la taille de la zone à partir de laquelle on fait respawn de l'autre coté
                if (image.transform.position.x < -_cameraHalfWidth - image.bounds.size.x/2)
                {
                    image.transform.position += image.bounds.size.x * 2 * Vector3.right;
                }
            }
        }

    }
}
