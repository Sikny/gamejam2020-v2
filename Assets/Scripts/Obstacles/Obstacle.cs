﻿using Global;
using Player;
using UnityEngine;

namespace Obstacles {
    public class Obstacle : MonoBehaviour
    {
        private Wheel _wheel;
        public GameManager gameManager;

        public void SetWheel(Wheel wheel) {
            _wheel = wheel;
        }

        private void Update()
        {
            transform.position += new Vector3(-gameManager.globalSpeed, 0)*Time.deltaTime;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer == 8) {
                _wheel.SetHole(collision.GetContact(0).point);
            }
            Destroy(gameObject);
        }
    }
}
