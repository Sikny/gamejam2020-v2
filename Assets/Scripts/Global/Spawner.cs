﻿using System.Collections;
using Obstacles;
using Parallax;
using Player;
using UnityEngine;
using Random = UnityEngine.Random;

// ReSharper disable RedundantDefaultMemberInitializer

namespace Global {
   public class Spawner : MonoBehaviour
   {
      [SerializeField] private float minFrequency = 0.2f;
      [SerializeField] private float maxFrequency = 2f;
      private Move _root;
      [SerializeField] private Obstacle[] prefabToSpawns = null;
      [SerializeField] private Wheel wheel = null;
      public GameManager gameManager;
      
      private float _lastSpawnTime;

      private void Awake()
      {
         _root = GetComponentInParent<Move>();
      }

      private void Start()
      {
         StartCoroutine(Spawning());
      }
      /*public void FixedUpdateLoop() {
         _lastSpawnTime += Time.deltaTime;
         if (!(_lastSpawnTime >= period)) return;
         _lastSpawnTime -= period;
         SpawnRandomObstacles();
      }*/

      void SpawnRandomObstacles() {
         Obstacle spawn = Instantiate(prefabToSpawns[Random.Range(0, prefabToSpawns.Length)]);
         spawn.transform.localPosition = transform.position;
         spawn.gameManager = gameManager;
         spawn.SetWheel(wheel);
      }

      IEnumerator Spawning()
      {
         while (true)
         {
            SpawnRandomObstacles();
            yield return new WaitForSeconds(Random.Range(minFrequency, maxFrequency));
         }
      }
   }
}


