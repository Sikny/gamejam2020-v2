﻿using Player;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

// ReSharper disable RedundantDefaultMemberInitializer

namespace Global {
    public class GameManager : MonoBehaviour {
        [SerializeField] private GameObject mainMenuCanvas = null;
        [SerializeField] private GameObject hudCanvas = null;
        [SerializeField] private HoleManager holeManager = null;
        [SerializeField] private Wheel wheel = null;
        [SerializeField] private Mask wheelMask = null;

        [HideInInspector] public string minutes;
        [HideInInspector] public string seconds;
        private float startTime;

        public float globalSpeed = 1f;
        private float startSpeed = 1f;
        public float increasingSpeedCoeff = 0.1f;
        
        public UnityEvent updateLoop;
        public UnityEvent fixedUpdateLoop;

        [HideInInspector] public bool gameHasStarted;

        void Start()
        {
            startSpeed = globalSpeed;
        }
        
        void Update() {
            if (!gameHasStarted) return;
            updateLoop.Invoke();
            Timer();
            globalSpeed += increasingSpeedCoeff * Time.deltaTime;
        }
    
        void FixedUpdate() {
            if (!gameHasStarted) return;
            fixedUpdateLoop.Invoke();
        }

        public void StartGame() {
            gameHasStarted = true;
            mainMenuCanvas.SetActive(false);
            hudCanvas.SetActive(true);
            hudCanvas.GetComponentInChildren<Crosses>().Reset();
            holeManager.Reset();
            wheel.Reset();
            wheelMask.Reset();
            globalSpeed = startSpeed;
            seconds = "";
            minutes = "";
            startTime = Time.time;

        }
        
        public void LooseGame() {
            gameHasStarted = false;
            mainMenuCanvas.SetActive(true);
            hudCanvas.SetActive(false);
            GetComponent<AudioSource>().Play();
        }

        private void Timer()
        {
            float t = Time.time - startTime;
            minutes = ((int)t / 60).ToString ();
            seconds = (t % 60).ToString ("f0");
            hudCanvas.GetComponentInChildren<Text>().text = minutes.ToString() + " : " + seconds.ToString();
        }

    }
}
