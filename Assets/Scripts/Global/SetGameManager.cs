﻿using System.Collections;
using System.Collections.Generic;
using Global;
using Player;
using UnityEngine;

public class SetGameManager : MonoBehaviour
{
    public Wheel wheel;
    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        wheel.gameManager = gameManager;
    }
}
