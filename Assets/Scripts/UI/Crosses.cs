﻿using System;
using System.Collections;
using System.Collections.Generic;
using Global;
using UnityEngine;
using UnityEngine.UI;

public class Crosses : MonoBehaviour
{
    [HideInInspector] public int chances = 3;
    private Image[] crosses;
    [SerializeField] private GameManager _gameManager;

    public void Reset()
    {
        crosses = GetComponentsInChildren<Image>();
        foreach (Image cross in crosses)
        {
            cross.gameObject.SetActive(false);
        }

        chances = 3;
    }

    public void MissClick()
    {
        chances -= 1;
        if (chances == 0) _gameManager.LooseGame();
        crosses[crosses.Length-chances-1].gameObject.SetActive(true);

    }

}
